# Utilio Password Utils

Set of utilities to easily analyse and generate passwords.

## Getting Started

### Installing


```
npm install utilio-password
```

## Modules

### Generation

Allows to generate random passwords following the specified rules. The rules available are stored in *generationRules* object.

Usage example:

```
const {generate, generationRules} = require('utilio-password');
generate(10, [generationRules.LOWERCASE, generationRules.UPPERCASE])
```

### Strength

Analyses the strength of a provided password. It uses entropy algorithm to calculate the password strength.

Usage example:

```
const {strength} = require('utilio-password');
strength("ab&FdAf#1@")
```



## Authors

* José Miguel Melo
