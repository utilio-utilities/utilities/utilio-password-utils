const { isRuleArrayValid, passwordContainsAnyCharacter, passwordRulesMatch } = require('./rules');

const {
  MIN_LENGTH, rules, characters, charactersAsArray,
} = require('./constants');

/**
 * shuffle
 * Shuffle string passed as parameter
 *
 * @name shuffle
 * @function
 * @param {String} str String to shuffle
 * @returns {String} Shuffled version of input string
 */
const shuffle = (str) => {
  const charactersList = str.split('');
  const charactersListLength = charactersList.length;

  for (let i = charactersListLength - 1; i > 0; i -= 1) {
    const j = Math.floor(Math.random() * (i + 1));
    const tmp = charactersList[i];
    charactersList[i] = charactersList[j];
    charactersList[j] = tmp;
  }

  return charactersList.join('');
};

/**
 * random
 * Generate random number between 0 and max
 *
 * @name random
 * @function
 * @param {Number} max Max limit
 * @returns {Number} Random number between 0 and max
 */
const random = max => Math.floor((Math.random() * max) + 0);


/**
 * generateInitialPasswordToFulfillRequirements
 * Generate a string which fulfills the specified rules array.
 *
 * @name generateInitialPasswordToFulfillRequirements
 * @function
 * @param {Array} rulesArray Array of rules to apply to the password
 * @returns {String} String which fulfills specified rules array
 */
const generateInitialPasswordToFulfillRequirements = (rulesArray) => {
  let password = '';

  if (rulesArray.includes(rules.UPPERCASE)) {
    const position = random(characters[rules.UPPERCASE].length);
    password += characters[rules.UPPERCASE][position];
  }
  if (rulesArray.includes(rules.LOWERCASE)) {
    const position = random(characters[rules.LOWERCASE].length);
    password += characters[rules.LOWERCASE][position];
  }
  if (rulesArray.includes(rules.SPECIAL_CHARACTERS)) {
    const position = random(characters[rules.SPECIAL_CHARACTERS].length);
    password += characters[rules.SPECIAL_CHARACTERS][position];
  }
  if (rulesArray.includes(rules.NUMBERS)) {
    const position = random(characters[rules.NUMBERS].length);
    password += characters[rules.NUMBERS][position];
  }

  return password;
};


/**
 * generate
 * Generate random password
 *
 * @name generate
 * @function
 * @param {Number} length Password length (default: 6)
 * @param {Array} rulesArray Array of rules to apply to the password
 * @returns {String} Generated password
 */
const generate = (
  length = MIN_LENGTH,
  rulesArray = [
    rules.UPPERCASE,
    rules.LOWERCASE,
    rules.NUMBERS,
    rules.SPECIAL_CHARACTERS,
  ],
) => {
  if (length < MIN_LENGTH) throw new Error(`Password length is too small. Minimum is ${MIN_LENGTH}.`);

  if (!isRuleArrayValid(rulesArray)) throw new Error(`Rules array has some invalid option. Should be one of the following: ${rules}`);

  let password = generateInitialPasswordToFulfillRequirements(rulesArray);

  while (password.length < length) {
    const charType = random(rulesArray.length);
    const typeList = characters[rulesArray[charType]];
    const position = random(typeList.length);
    password += typeList[position];
  }

  return shuffle(password);
};

module.exports = {
  passwordCharacters: characters,
  passwordCharactersAsArray: charactersAsArray,
  generate,
  generationRules: rules,
  passwordRulesMatch,
  passwordContainsAnyCharacter,
};
