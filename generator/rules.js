const { MIN_LENGTH, rules, charactersAsArray } = require('./constants');

/**
 * isRuleArrayValid
 * Validate rule array specified to check if all values are allowed
 *
 * @name isRuleArrayValid
 * @function
 * @param {Array} rulesArray Array of rules to verify
 * @returns {Boolean} Valid or not?
 */
const isRuleArrayValid = (rulesArray) => {
  if (rulesArray === undefined || rulesArray === null) {
    throw new Error('Rules array is undefined');
  }

  if (rulesArray.length === 0) return false;

  return rulesArray.filter(v => Object.values(rules).indexOf(v) === -1).length === 0;
};

/**
 * Check if password contains any character.
 * @param {String} password
 * @param {Array} charsArray
 * @returns {Boolean} If password contains any of the characters in charsArray,
 *  then it returns true. False otherwise.
 */
const passwordContainsAnyCharacter = (password, charsArray) => {
  let ruleExists = false;
  charsArray.forEach((v) => {
    if (password.indexOf(v) !== -1) ruleExists = true;
  });
  return ruleExists;
};

/**
 * Check all rules that a password match
 * @param {String} password Password to use to check rules
 */
const passwordRulesMatch = (password) => {
  const rulesMatches = {
    length: false,
    uppercase: false,
    lowercase: false,
    number: false,
    special_characters: false,
  };

  // Length rule
  if (password.length > MIN_LENGTH) {
    rulesMatches.length = true;
  }
  // Uppercase rule
  if (passwordContainsAnyCharacter(password, charactersAsArray[rules.UPPERCASE])) {
    rulesMatches.uppercase = true;
  }
  // Lowercase rule
  if (passwordContainsAnyCharacter(password, charactersAsArray[rules.LOWERCASE])) {
    rulesMatches.lowercase = true;
  }
  // Numbers rule
  if (passwordContainsAnyCharacter(password, charactersAsArray[rules.NUMBERS])) {
    rulesMatches.number = true;
  }
  // Special characters rule
  if (passwordContainsAnyCharacter(password, charactersAsArray[rules.SPECIAL_CHARACTERS])) {
    rulesMatches.special_characters = true;
  }

  return rulesMatches;
};

module.exports = {
  isRuleArrayValid,
  passwordRulesMatch,
  passwordContainsAnyCharacter,
};
