const MIN_LENGTH = 6;

/**
 * rules
 * Object with all rules available.
 *
 * @name rules
 * @object
 */
const rules = {
  UPPERCASE: 'uppercase',
  LOWERCASE: 'lowercase',
  NUMBERS: 'numbers',
  SPECIAL_CHARACTERS: 'special_characters',
};

const characters = {
  [rules.UPPERCASE]: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
  [rules.LOWERCASE]: 'abcdefghijklmnopqrstuvwxyz',
  [rules.NUMBERS]: '0123456789',
  [rules.SPECIAL_CHARACTERS]: '@#_!$%&()*+,-./:;<=>?^`{|}~',
};

const charactersAsArray = {
  [rules.UPPERCASE]: characters[rules.UPPERCASE].split(''),
  [rules.LOWERCASE]: characters[rules.LOWERCASE].split(''),
  [rules.NUMBERS]: characters[rules.NUMBERS].split(''),
  [rules.SPECIAL_CHARACTERS]: characters[rules.SPECIAL_CHARACTERS].split(''),
};


module.exports = {
  MIN_LENGTH,
  rules,
  characters,
  charactersAsArray,
};
