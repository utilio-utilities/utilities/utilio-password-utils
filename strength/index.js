const { entropy } = require('./entropy');

/**
 * strengthRules
 * Rules to determine password strength
 */
exports.strengthRules = {
  very_weak: {
    id: 'very_weak',
    description: 'Very weak',
    minEntropy: 0,
    maxEntropy: 15,
  },
  weak: {
    id: 'weak',
    description: 'Weak',
    minEntropy: 15,
    maxEntropy: 30,
  },
  medium: {
    id: 'medium',
    description: 'Medium',
    minEntropy: 30,
    maxEntropy: 45,
  },
  strong: {
    id: 'strong',
    description: 'Strong',
    minEntropy: 45,
    maxEntropy: 55,
  },
  very_strong: {
    id: 'very_strong',
    description: 'Very string',
    minEntropy: 55,
    maxEntropy: 1000,
  },
};

/**
 * strength
 * Calculate password strength
 *
 * @name strength
 * @function
 * @param {String} password Password to be analyzed
 * @returns {String} Password strength (very_weak, weak, medium, strong, very_strong)
 */
exports.strength = (password) => {
  const entropyValue = entropy(password);

  if (entropyValue < this.strengthRules.very_weak.maxEntropy) {
    return this.strengthRules.very_weak.id;
  }
  if (entropyValue < this.strengthRules.weak.maxEntropy) { return this.strengthRules.weak.id; }
  if (entropyValue < this.strengthRules.medium.maxEntropy) { return this.strengthRules.medium.id; }
  if (entropyValue < this.strengthRules.strong.maxEntropy) { return this.strengthRules.strong.id; }
  return this.strengthRules.very_strong.id;
};
