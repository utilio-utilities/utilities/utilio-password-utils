const numberLowercaseLetters = 26;
const numberUppercaseLetters = 26;
const numberDigits = 10;
const numberSpecialCharacters = 32;

const entropyFactor = 0.25;
const egfUppercase = 0.4;
const egfDigits = 0.4;
const egfSpecialChars = 0.5;

/**
 * entropy
 * Calculate password entropy
 *
 * @name entropy
 * @function
 * @param {String} password Password to be analyzed
 * @returns {Number} Password entropy
 */
exports.entropy = (password) => {
  const { length } = password;

  // unique character counts from password (duplicates discarded)
  const uqlca = (password.match(/[a-z]/g, '') || [])
    .filter((item, i, ar) => ar.indexOf(item) === i).join('').length;
  const uquca = (password.match(/[A-Z]/g, '') || [])
    .filter((item, i, ar) => ar.indexOf(item) === i).join('').length;
  const uqd = (password.match(/[0-9]/g, '') || [])
    .filter((item, i, ar) => ar.indexOf(item) === i).join('').length;
  const uqsp = (password.match(/[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?]/gi, '') || [])
    .filter((item, i, ar) => ar.indexOf(item) === i).join('').length;

  // repetition factors.  few unique letters == low factor, many unique == high
  const rflca = (1 - ((1 - entropyFactor) ** uqlca));
  const rfuca = (1 - ((1 - egfUppercase) ** uquca));
  const rfd = (1 - ((1 - egfDigits) ** uqd));
  const rfsp = (1 - ((1 - egfSpecialChars) ** uqsp));

  // digit strengths
  const strength = (rflca * numberLowercaseLetters
    + rfuca * numberUppercaseLetters
    + rfd * numberDigits
    + rfsp * numberSpecialCharacters) ** length;

  return Math.log2(strength);
};
