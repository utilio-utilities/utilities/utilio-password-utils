/* eslint-disable no-undef */
const { expect } = require('chai');
const {
  isRuleArrayValid,
  passwordContainsAnyCharacter,
  passwordRulesMatch,
} = require('../../generator/rules');
const { rules } = require('../../generator/constants');

describe('Password generation rules', () => {
  it('must throw an exception when rules array is undefined', () => {
    expect(() => isRuleArrayValid()).to.throw();
  });

  it('must throw an exception when rules array is empty', () => {
    /* eslint-disable-next-line */
    expect(isRuleArrayValid([])).to.be.false;
  });

  it('must return true when array contains valid options', () => {
    /* eslint-disable-next-line */
    expect(isRuleArrayValid([rules.UPPERCASE])).to.be.true;
    /* eslint-disable-next-line */
    expect(isRuleArrayValid([rules.UPPERCASE, rules.LOWERCASE])).to.be.true;
    /* eslint-disable-next-line */
    expect(isRuleArrayValid([
      rules.UPPERCASE,
      rules.LOWERCASE,
      rules.NUMBERS,
    ])).to.be.true;
    /* eslint-disable-next-line */
    expect(isRuleArrayValid([
      rules.UPPERCASE,
      rules.LOWERCASE,
      rules.NUMBERS,
      rules.SPECIAL_CHARACTERS,
    ])).to.be.true;
  });

  it('must return false when array contains invalid option', () => {
    /* eslint-disable-next-line */
    expect(isRuleArrayValid([rules.UPPERCASE, 'test'])).to.be.false;
    /* eslint-disable-next-line */
    expect(isRuleArrayValid(['test'])).to.be.false;
  });
});

describe('Password rules checker', () => {
  it('must return true when character of type exists in password', () => {
    /* eslint-disable-next-line */
    expect(passwordContainsAnyCharacter('123kakcm', ['a'])).to.be.true;
    /* eslint-disable-next-line */
    expect(passwordContainsAnyCharacter('123kakcm@', ['1'])).to.be.true;
    /* eslint-disable-next-line */
    expect(passwordContainsAnyCharacter('123kakcm@', ['@'])).to.be.true;
  });
  it('must return false when character of type does not exists in password', () => {
    /* eslint-disable-next-line */
    expect(passwordContainsAnyCharacter('123kakcm', ['b'])).to.be.false;
    /* eslint-disable-next-line */
    expect(passwordContainsAnyCharacter('123kakcm@', ['5'])).to.be.false;
    /* eslint-disable-next-line */
    expect(passwordContainsAnyCharacter('123kakcm@', ['&'])).to.be.false;
  });

  it('must return only numbers as true', () => {
    const numberedPassword = passwordRulesMatch('1112');
    /* eslint-disable-next-line */
    expect(numberedPassword.number).to.be.true;
    /* eslint-disable-next-line */
    expect(numberedPassword.lowercase).to.be.false;
    /* eslint-disable-next-line */
    expect(numberedPassword.uppercase).to.be.false;
    /* eslint-disable-next-line */
    expect(numberedPassword.special_characters).to.be.false;
    /* eslint-disable-next-line */
    expect(numberedPassword.length).to.be.false;
  });
  it('must return numbers and lowercase as true', () => {
    const numberedPassword = passwordRulesMatch('111a');
    /* eslint-disable-next-line */
    expect(numberedPassword.number).to.be.true;
    /* eslint-disable-next-line */
    expect(numberedPassword.lowercase).to.be.true;
    /* eslint-disable-next-line */
    expect(numberedPassword.uppercase).to.be.false;
    /* eslint-disable-next-line */
    expect(numberedPassword.special_characters).to.be.false;
    /* eslint-disable-next-line */
    expect(numberedPassword.length).to.be.false;
  });
  it('must return numbers, lowercase and uppercase as true', () => {
    const numberedPassword = passwordRulesMatch('111aA');
    /* eslint-disable-next-line */
    expect(numberedPassword.number).to.be.true;
    /* eslint-disable-next-line */
    expect(numberedPassword.lowercase).to.be.true;
    /* eslint-disable-next-line */
    expect(numberedPassword.uppercase).to.be.true;
    /* eslint-disable-next-line */
    expect(numberedPassword.special_characters).to.be.false;
    /* eslint-disable-next-line */
    expect(numberedPassword.length).to.be.false;
  });
  it('must return numbers, lowercase, uppercase and special chars as true', () => {
    const numberedPassword = passwordRulesMatch('11@aA');
    /* eslint-disable-next-line */
    expect(numberedPassword.number).to.be.true;
    /* eslint-disable-next-line */
    expect(numberedPassword.lowercase).to.be.true;
    /* eslint-disable-next-line */
    expect(numberedPassword.uppercase).to.be.true;
    /* eslint-disable-next-line */
    expect(numberedPassword.special_characters).to.be.true;
    /* eslint-disable-next-line */
    expect(numberedPassword.length).to.be.false;
  });
  it('must return all rules as true', () => {
    const numberedPassword = passwordRulesMatch('11bs@aA');
    /* eslint-disable-next-line */
    expect(numberedPassword.number).to.be.true;
    /* eslint-disable-next-line */
    expect(numberedPassword.lowercase).to.be.true;
    /* eslint-disable-next-line */
    expect(numberedPassword.uppercase).to.be.true;
    /* eslint-disable-next-line */
    expect(numberedPassword.special_characters).to.be.true;
    /* eslint-disable-next-line */
    expect(numberedPassword.length).to.be.true;
  });
});
