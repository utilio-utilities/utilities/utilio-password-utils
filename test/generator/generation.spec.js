/* eslint-disable no-undef */
const { expect } = require('chai');
const { generate } = require('../../generator/index');
const { rules, characters } = require('../../generator/constants');

const passwordDefaultLength = 6;

checkRules = (password) => {
  let containsUppercase = false;
  let containsLowercase = false;
  let containsNumbers = false;
  let containsSpecialChars = false;
  for (let i = 0; i < password.length; i += 1) {
    if (password[i] === password[i].toUpperCase()) containsUppercase = true;
    if (password[i] === password[i].toLowerCase()) containsLowercase = true;
    if (characters[rules.NUMBERS].indexOf(password[i]) !== -1) containsNumbers = true;
    if (characters[rules.SPECIAL_CHARACTERS].indexOf(password[i]) !== -1) {
      containsSpecialChars = true;
    }
  }
  return {
    uppercase: containsUppercase,
    lowercase: containsLowercase,
    numbers: containsNumbers,
    special_characters: containsSpecialChars,
  };
};


describe('Password generation', () => {
  it('must create uppercase password', () => {
    const password = generate(passwordDefaultLength, [rules.UPPERCASE]);
    const pwdRules = checkRules(password);
    /* eslint-disable-next-line */
    expect(pwdRules.uppercase).to.be.true;
    /* eslint-disable-next-line */
    expect(password === password.toUpperCase()).to.be.true;
  });

  it('must create lowercase password', () => {
    const password = generate(passwordDefaultLength, [rules.LOWERCASE]);
    const pwdRules = checkRules(password);
    /* eslint-disable-next-line */
    expect(pwdRules.lowercase).to.be.true;
    /* eslint-disable-next-line */
    expect(password === password.toLowerCase()).to.be.true;
  });

  it('must create numbers password', () => {
    const password = generate(passwordDefaultLength, [rules.NUMBERS]);
    const pwdRules = checkRules(password);
    /* eslint-disable-next-line */
    expect(pwdRules.numbers).to.be.true;
  });

  it('must create special characters password', () => {
    const password = generate(passwordDefaultLength, [rules.SPECIAL_CHARACTERS]);
    const pwdRules = checkRules(password);
    /* eslint-disable-next-line */
    expect(pwdRules.special_characters).to.be.true;
  });

  it('must create uppercase, lowercase password', () => {
    const password = generate(passwordDefaultLength, [rules.UPPERCASE, rules.LOWERCASE]);
    const pwdRules = checkRules(password);

    /* eslint-disable-next-line */
    expect(pwdRules.uppercase).to.be.true;
    /* eslint-disable-next-line */
    expect(pwdRules.lowercase).to.be.true;
  });

  it('must create uppercase, lowercase, numbers password', () => {
    const password = generate(passwordDefaultLength, [
      rules.UPPERCASE, rules.LOWERCASE, rules.NUMBERS,
    ]);
    const pwdRules = checkRules(password);


    /* eslint-disable-next-line */
    expect(pwdRules.uppercase).to.be.true;
    /* eslint-disable-next-line */
    expect(pwdRules.lowercase).to.be.true;
    /* eslint-disable-next-line */
    expect(pwdRules.numbers).to.be.true;
  });

  it('must create uppercase, lowercase, numbers, special chars password', () => {
    const password = generate(passwordDefaultLength, [
      rules.UPPERCASE, rules.LOWERCASE, rules.NUMBERS, rules.SPECIAL_CHARACTERS,
    ]);
    const pwdRules = checkRules(password);
    /* eslint-disable-next-line */
    expect(pwdRules.uppercase).to.be.true;
    /* eslint-disable-next-line */
    expect(pwdRules.lowercase).to.be.true;
    /* eslint-disable-next-line */
    expect(pwdRules.numbers).to.be.true;
    /* eslint-disable-next-line */
    expect(pwdRules.special_characters).to.be.true;
  });

  it('must create password with default settings', () => {
    const password = generate();
    const pwdRules = checkRules(password);

    /* eslint-disable-next-line */
    expect(pwdRules.uppercase).to.be.true;
    /* eslint-disable-next-line */
    expect(pwdRules.lowercase).to.be.true;
    /* eslint-disable-next-line */
    expect(pwdRules.numbers).to.be.true;
    /* eslint-disable-next-line */
    expect(pwdRules.special_characters).to.be.true;

    /* eslint-disable-next-line */
    expect(password.length).to.equal(passwordDefaultLength);
  });

  it('must create password with default rules', () => {
    const length = 12;
    const password = generate(length);

    const pwdRules = checkRules(password);
    /* eslint-disable-next-line */
    expect(pwdRules.uppercase).to.be.true;
    /* eslint-disable-next-line */
    expect(pwdRules.lowercase).to.be.true;
    /* eslint-disable-next-line */
    expect(pwdRules.numbers).to.be.true;
    /* eslint-disable-next-line */
    expect(pwdRules.special_characters).to.be.true;

    /* eslint-disable-next-line */
    expect(password.length).to.equal(length);
  });

  it('must create password with specific length', () => {
    const length = 12;
    const password = generate(length, [
      rules.UPPERCASE, rules.LOWERCASE, rules.NUMBERS, rules.SPECIAL_CHARACTERS,
    ]);

    /* eslint-disable-next-line */
    expect(password.length).to.equal(length);
  });

  it('must throw exception when invalid rule array', () => {
    const length = 12;

    try {
      generate(length, [
        rules.UPPERCASE, rules.LOWERCASE, rules.NUMBERS, rules.SPECIAL_CHARACTERS, 'not_valid',
      ]);
      /* eslint-disable-next-line */
        expect(false).to.be.true;
    } catch (e) {
      /* eslint-disable-next-line */
        expect(e).not.to.be.null;
    }
  });

  it('must throw exception when length lower than min length', () => {
    const length = 2;
    try {
      generate(length);
      /* eslint-disable-next-line */
      expect(false).to.be.true;
    } catch (e) {
      /* eslint-disable-next-line */
      expect(e).not.to.be.null;
    }
  });
});
