/* eslint-disable no-undef */
const { expect } = require('chai');
const { strength, strengthRules } = require('../../strength');

describe('Password strength', () => {
  it('must return very weak password', () => {
    const s = strength('aaa');
    expect(s).to.equal(strengthRules.very_weak.id);
  });
  it('must return weak password', () => {
    s = strength('abcdef');
    expect(s).to.equal(strengthRules.weak.id);
  });
  it('must return medium password', () => {
    s = strength('abcdef#1');
    expect(s).to.equal(strengthRules.medium.id);
  });
  it('must return strong password', () => {
    s = strength('ab&FdAf#');
    expect(s).to.equal(strengthRules.strong.id);
  });
  it('must return very strong password', () => {
    s = strength('ab&F%JLz98dAf#1@');
    expect(s).to.equal(strengthRules.very_strong.id);
  });
});
