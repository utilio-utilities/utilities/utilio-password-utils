/* eslint-disable no-undef */
const { expect } = require('chai');
const { entropy } = require('../../strength/entropy');

describe('Password strength entropy algorithm', () => {
  it('must return correct entropy values', () => {
    let pEntropy = entropy('aaa');
    let pEntropyRounded = Math.round(pEntropy * 10) / 10;
    expect(pEntropyRounded).to.equal(8.1);

    pEntropy = entropy('AB');
    pEntropyRounded = Math.round(pEntropy * 10) / 10;
    expect(pEntropyRounded).to.equal(8.1);

    pEntropy = entropy('aaaaaaaaa');
    pEntropyRounded = Math.round(pEntropy * 10) / 10;
    expect(pEntropyRounded).to.equal(24.3);

    pEntropy = entropy('H0ley$Mol3y_');
    pEntropyRounded = Math.round(pEntropy * 10) / 10;
    expect(pEntropyRounded).to.equal(72.2);
  });
});
